FROM busybox

COPY covered /usr/local/bin/covered
WORKDIR /data
ENTRYPOINT ["/usr/local/bin/covered"]